package com.learn.docker.learndocker.services;

import com.learn.docker.learndocker.entities.Employee;
import com.learn.docker.learndocker.repositories.EmployeeRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public EmployeeService(final EmployeeRepository employeeRepository) {

        this.employeeRepository = employeeRepository;
    }

    public Employee get(Integer id) {

        return employeeRepository.getOne(id);

    }
}
