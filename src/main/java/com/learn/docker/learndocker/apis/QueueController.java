package com.learn.docker.learndocker.apis;

import com.learn.docker.learndocker.queue.Producer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QueueController {

    private final Producer producer;

    public QueueController(final Producer producer) {

        this.producer = producer;
    }

    @PostMapping("/queue")
    public ResponseEntity<String> get(@RequestBody String message) {

        producer.produce(message);
        return new ResponseEntity<>("Queued Successfully", HttpStatus.OK);
    }


}
