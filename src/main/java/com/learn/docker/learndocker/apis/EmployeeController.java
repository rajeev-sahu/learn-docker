package com.learn.docker.learndocker.apis;

import com.learn.docker.learndocker.entities.Employee;
import com.learn.docker.learndocker.services.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(final EmployeeService employeeService) {

        this.employeeService = employeeService;
    }

    @GetMapping("/emp/{emp-id}")
    public ResponseEntity<EmployeeDetailResponse> get(@PathVariable("emp-id") Integer empId) {

        Employee employee = employeeService.get(empId);
        return new ResponseEntity<>(new EmployeeDetailResponse(employee.getId(), employee.getSalary(), employee.getName()), HttpStatus.OK);
    }


}
