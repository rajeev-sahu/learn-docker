package com.learn.docker.learndocker.apis;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
class EmployeeDetailResponse {

    private final Integer id;

    private final Double salary;

    private final String name;

    EmployeeDetailResponse(final Integer id,
                           final Double salary,
                           final String name) {

        this.id = id;
        this.salary = salary;
        this.name = name;
    }
}
