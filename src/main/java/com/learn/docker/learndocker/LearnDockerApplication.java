package com.learn.docker.learndocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class LearnDockerApplication {

    public static void main(String[] args) {

        SpringApplication.run(LearnDockerApplication.class, args);
    }
}
