package com.learn.docker.learndocker.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "employees" )
@Data
public class Employee {

    @Id
    @Column(name = "emp_id")
    private Integer id;

    private Double salary;

    private String name;
}
