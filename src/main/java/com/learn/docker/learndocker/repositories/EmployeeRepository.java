package com.learn.docker.learndocker.repositories;

import com.learn.docker.learndocker.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
