package com.learn.docker.learndocker.queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.TextMessage;

@Component
public class Producer {

    private final JmsTemplate jmsTemplate;

    @Autowired
    public Producer(final JmsTemplate jmsTemplate) {

        this.jmsTemplate = jmsTemplate;
    }

    public void produce(final String message) {

        jmsTemplate.execute((session, messageProducer) ->
                            {
                                final TextMessage textMessage = session.createTextMessage(message);
                                messageProducer.send(new ActiveMQQueue("test-new-spring-queue"), textMessage);
                                return textMessage.getText();
                            });
    }
}
