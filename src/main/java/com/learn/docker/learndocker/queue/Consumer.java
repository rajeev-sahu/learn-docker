package com.learn.docker.learndocker.queue;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.TextMessage;


@Component
public class Consumer {

    @JmsListener(destination = "test-new-spring-queue")
    public void consume(TextMessage message) {

        try {
            System.out.println("I have consumed message." + message.getText());
        }
        catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
