#FROM openjdk:8-jdk-alpine
#EXPOSE 8080 8080
#VOLUME /tmp
#ADD target/learn-docker-0.0.1-SNAPSHOT.jar target/app.jar
##ARG JAR_FILE
##COPY ${JAR_FILE} app.jar
##"-Djava.security.egd=file:/dev/./urandom",
#RUN /bin/sh -c 'touch target/app.jar'
#ENTRYPOINT ["java","-jar","/app.jar"]

FROM openjdk:8-jdk-alpine
#VOLUME /tmp
ADD target/learn-docker-0.0.1-SNAPSHOT.jar app.jar
#RUN /bin/sh -c 'touch target/app.jar'
EXPOSE 8080
#EXPOSE 8161
#EXPOSE 61616
#ENTRYPOINT ["java","-jar","app.jar"]
CMD ["java","-jar","app.jar"]