CREATE DATABASE learn;

CREATE TABLE employees
(
  emp_id INTEGER,
  name TEXT,
  salary NUMERIC(10,2)
);

INSERT INTO employees VALUES (1,'Rajeev',10000),(2,'Rajiv',20000),(3,'Raju',30000),(4,'Sahu',40000);